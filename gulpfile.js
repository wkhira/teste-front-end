var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var jshint = require('gulp-jshint');
var jshintStylish = require('jshint-stylish');
var csslint = require('gulp-csslint');

/* Sass */
gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('src/css'))
        .pipe(reload({stream:true}));
});

/* Browser Sync */
gulp.task('browser-sync', function() {
    browserSync.init({
		server: {
			baseDir: 'src'
		}
    });
});

/* Watchers */
gulp.task('default', ['sass', 'browser-sync'], function () {
    gulp.watch('src/scss/*.scss',['sass']);

    gulp.watch('src/js/*.js').on('change', function(event) {
        gulp.src(event.path)
            .pipe(jshint())
            .pipe(jshint.reporter(jshintStylish));
    });

    gulp.watch('src/css/*.css').on('change', function(event) {
        gulp.src(event.path)
            .pipe(csslint())
            .pipe(csslint.formatter());
    });

    gulp.watch('src/**/*').on('change', browserSync.reload);
});

